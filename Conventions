no.1 Comparison
__________________________________
data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override fun compareTo(other: MyDate) = when {
        year != other.year -> year - other.year
        month != other.month -> month - other.month
        else -> dayOfMonth - other.dayOfMonth
    }
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}
__________________________________

no.2 Ranges
__________________________________
fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in first..last
}
__________________________________

no.3 For loop
__________________________________
class DateRange(val start: MyDate, val end: MyDate) : Iterable<MyDate> {
    override fun iterator(): Iterator<MyDate> {
        return object : Iterator<MyDate> {
            var current: MyDate = start
            override fun next(): MyDate {
                if (!hasNext()) throw NoSuchElementException()
                val result = current
                current = current.followingDate()
                return result
            }
            override fun hasNext(): Boolean = current <= end
        }
    }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}
__________________________________

no.4 Operators overloading
__________________________________
import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

// Supported intervals that might be added to dates:
enum class TimeInterval { DAY, WEEK, YEAR }

operator fun MyDate.plus(timeInterval: TimeInterval): MyDate {
    return addTimeIntervals(timeInterval, 1)
}

fun task1(today: MyDate): MyDate {
    return today + YEAR + WEEK
}

// Extra class to support adding several time intervals to a date
class MultiTimeInterval(val intervals: List<Pair<TimeInterval, Int>>) {
    operator fun TimeInterval.times(number: Int) = Pair(this, number)
}

operator fun MyDate.plus(multiInterval: MultiTimeInterval): MyDate {
    var result = this
    for ((interval, number) in multiInterval.intervals) {
        result = result.addTimeIntervals(interval, number)
    }
    return result
}

fun task2(today: MyDate): MyDate {
     return today + MultiTimeInterval(listOf(YEAR to 2, WEEK to 3, DAY to 5))
}
__________________________________

no.5 Invoke
__________________________________
class Invokable {
    var numberOfInvocations: Int = 0
        private set

    operator fun invoke(): Invokable {
        numberOfInvocations++
        return this
    }
}

fun invokeTwice(invokable: Invokable) = invokable()()
__________________________________
